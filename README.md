# demo-alexa
AWS Alexa demo.


## References

* [Alexa Skills Kit](https://developer.amazon.com/alexa-skills-kit)
* [Alexa Skills Kit SDK for Node.js](https://github.com/alexa/alexa-skills-kit-sdk-for-nodejs)
* [Alexa Voice Service](https://developer.amazon.com/alexa-voice-service)

